package huypt.com.kotlinandroid.module.game.game_view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import huypt.com.kotlinandroid.R
import huypt.com.kotlinandroid.module.game.game_thread.GameThread
import huypt.com.kotlinandroid.module.game.model.Grenade
import huypt.com.kotlinandroid.module.game.model.Player

/*
 * Created by huypt1 on 5/23/2018.
 */
class GameView(context: Context, attributeSet: AttributeSet) : SurfaceView(context, attributeSet), SurfaceHolder.Callback {

    private lateinit var thread: GameThread
    private var grenade: Grenade? = null
    private var player: Player? = null

    private var touched: Boolean = false
    private var touched_x: Int = 0
    private var touched_y: Int = 0

    init {
        holder.addCallback(this)
        thread = GameThread(holder, this)
    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
    }

    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        val retry = true
        while (retry) {
            try {
                thread.setRunning(false)
                thread.join()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        //game object
        grenade = Grenade(BitmapFactory.decodeResource(resources, R.drawable.ic_angry))

        thread.setRunning(true)
        thread.start()
    }

    /*function to update the position of player and game objects */
    fun update() {
        grenade!!.update()
        if (!touched) {
            player!!.updateTouch(touched_x, touched_y)
        }
    }

    /*Everything that has to be draw on Canvas*/
    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        grenade!!.draw(canvas)
        player!!.draw(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        // when ever there is a touch on the screen,
        // we can get the position of touch
        // which we may use it for tracking some of the game objects
        touched_x = event.x.toInt()
        touched_y = event.y.toInt()

        val action = event.action
        when (action) {
            MotionEvent.ACTION_DOWN -> touched = true
            MotionEvent.ACTION_MOVE -> touched = true
            MotionEvent.ACTION_UP -> touched = false
            MotionEvent.ACTION_CANCEL -> touched = false
            MotionEvent.ACTION_OUTSIDE -> touched = false
        }
        return true
    }
}