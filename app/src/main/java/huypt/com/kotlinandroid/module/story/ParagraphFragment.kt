package huypt.com.kotlinandroid.module.story

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import huypt.com.kotlinandroid.R
import huypt.com.kotlinandroid.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_story_page.*
import android.webkit.WebChromeClient
import huypt.com.kotlinandroid.model.Stories


/*
 * Created by huypt1 on 6/21/2018.
 */
class ParagraphFragment : BaseFragment() {

    private var pos: Int = 0
    private lateinit var stories: Stories

    companion object {
        fun newInstance(stories: Stories): ParagraphFragment {
            val args = Bundle()
            val fragment = ParagraphFragment()
            fragment.stories = stories
            fragment.arguments = args
            return fragment
        }
    }

    override fun initData(bundle: Bundle?) {
        pos = bundle?.getInt("KEY")!!
    }

    override fun bindLayout() = R.layout.fragment_story_page

    @SuppressLint("SetJavaScriptEnabled")
    override fun initView() {
        val htmlSource = stories.htmlSource
        val webSettings = webview.settings
        webSettings.javaScriptEnabled = true


//        webview.webViewClient = MyWebViewClient(context)
        webview.loadUrl(htmlSource)
        webview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                progress_bar.progress = progress
                if (progress == 100)
                    progress_bar.visibility = View.GONE
                else
                    progress_bar.visibility = View.VISIBLE
            }
        }
    }
}
