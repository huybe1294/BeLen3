/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package huypt.com.kotlinandroid.module.for_parents.music

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.C.ContentType
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

import huypt.com.kotlinandroid.R
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error

/** Manages the [ExoPlayer], the IMA plugin and all video playback.  */
/* package */
class PlayerManager(context: Context?, var callBack: CallBack) : AnkoLogger {

    private val manifestDataSourceFactory: DataSource.Factory
    private val mediaDataSourceFactory: DataSource.Factory

    private var player: SimpleExoPlayer
    private var contentPosition: Long = 0

    init {
        manifestDataSourceFactory = DefaultDataSourceFactory(
                context, Util.getUserAgent(context, context?.getString(R.string.app_name)))
        mediaDataSourceFactory = DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context, context?.getString(R.string.app_name)),
                DefaultBandwidthMeter())

        // Create a default track selector.
        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        // Create a player instance.
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
    }

    fun init(context: Context?, playerView: PlayerView) {
        // Bind the player to the view.
        playerView.player = player
    }

    fun switchSong(songUrl: String?) {
        // This is the MediaSource representing the content media (i.e. not the ad).
        val mediaSource = buildMediaSource(Uri.parse(songUrl))
        // Prepare the player with the source.
        player.prepare(mediaSource)
        player.seekTo(contentPosition)
        player.playWhenReady = true
        player.addListener(object : Player.EventListener {
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
            }

            override fun onSeekProcessed() {
            }

            override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
            }

            override fun onLoadingChanged(isLoading: Boolean) {
            }

            override fun onPositionDiscontinuity(reason: Int) {
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_READY) {
                    callBack.getDuration(player.duration)
                    callBack.getCurrentPosition(player.currentPosition)
                    callBack.getPlayerStatus(player.playWhenReady)
                }
            }
        })
    }

    fun getCurrentPosition(): Long {
        return player.currentPosition
    }

    fun getBufferedPosition(): Long {
        return player.bufferedPosition
    }

    fun stop() {
        contentPosition = player.contentPosition
        player.stop(true)
    }

    fun release() {
        player.release()
    }

    // Internal methods.
    private fun buildMediaSource(uri: Uri): MediaSource {
        @ContentType val type = Util.inferContentType(uri)
        return when (type) {
            C.TYPE_DASH -> DashMediaSource.Factory(
                    DefaultDashChunkSource.Factory(mediaDataSourceFactory),
                    manifestDataSourceFactory)
                    .createMediaSource(uri)
            C.TYPE_SS -> SsMediaSource.Factory(
                    DefaultSsChunkSource.Factory(mediaDataSourceFactory), manifestDataSourceFactory)
                    .createMediaSource(uri)
            C.TYPE_HLS -> HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri)
            C.TYPE_OTHER -> ExtractorMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri)
            else -> throw IllegalStateException("Unsupported type: $type") as Throwable
        }
    }

    interface CallBack {
        fun getDuration(duration: Long)
        fun getCurrentPosition(currentPosition: Long)
        fun getPlayerStatus(isPlaying: Boolean)
    }
}
