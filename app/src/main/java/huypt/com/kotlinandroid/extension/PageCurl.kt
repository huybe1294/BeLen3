package huypt.com.kotlinandroid.extension

/*
 * Created by huypt1 on 6/21/2018.
 */
interface PageCurl {
    fun setCurlFactor(curl: Float)
}