package huypt.com.kotlinandroid

import android.app.Application
import com.google.firebase.database.FirebaseDatabase

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        instance = this
    }

    companion object {
        lateinit var instance: App
    }
}