package huypt.com.kotlinandroid.activity

import android.annotation.SuppressLint
import android.os.Build
import butterknife.OnClick
import huypt.com.kotlinandroid.R
import huypt.com.kotlinandroid.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import huypt.com.kotlinandroid.module.for_parents.AdultModeActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import org.jetbrains.anko.*

class MainActivity : BaseActivity() {

    private val TAG: String = "MainActivity"

    private lateinit var pageAdapter: MainPagerAdapter
    private lateinit var mInterstitialAd: InterstitialAd

    override fun initData(bundle: Bundle?) {
    }

    override fun bindLayout() = R.layout.activity_main

    override fun getContainerId() = R.id.fragmentContainer

    override fun initView() {
        Thread.setDefaultUncaughtExceptionHandler(Exception(this, MainActivity::class.java))

        //disable bottom navigation view shift mode
        navigation_bar.enableItemShiftingMode(false)
        navigation_bar.enableShiftingMode(false)
        navigation_bar.setIconSize(20f, 20f) //dp
        navigation_bar.setTextSize(12f) //sp

        pageAdapter = MainPagerAdapter(supportFragmentManager)
        vp_main.adapter = pageAdapter
        vp_main.offscreenPageLimit = 1

        navigation_bar.setupWithViewPager(vp_main)

        //event item bottom navigation clicked, lambda expression
        bottomNavigationItemClick()

        /*Init Admob*/
        MobileAds.initialize(this, "ca-app-pub-3940256099942544/6300978111")

        val adRequest = AdRequest.Builder().addTestDevice("5312860475028DD926C0CEE06C332950").build()
        mAdView.loadAd(adRequest)

        //Event admob listener
        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                longToast("load ads success")
            }

            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                longToast("load ads fail  $p0")
            }
        }

//        mInterstitialAd = InterstitialAd(this)
//        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
//        mInterstitialAd.loadAd(AdRequest.Builder().build())
//        //reload
//        mInterstitialAd.adListener = object : AdListener(){
//            override fun onAdClosed() {
//                mInterstitialAd.loadAd(AdRequest.Builder().build())
//            }
//        }
    }


    /* ============= EVENT CLICK ========== */

    @SuppressLint("Recycle")
    @RequiresApi(Build.VERSION_CODES.M)
    @OnClick(R.id.img_orther)
    internal fun adultMode() {
        startActivity<AdultModeActivity>()
    }

    @OnClick(R.id.mAdView)
    internal fun adMob() {
//        if (mInterstitialAd.isLoaded) {
//            mInterstitialAd.show()
//        } else {
//            toast("The interstitialAd wasn't loaded yet")
//        }
    }


    /*FUNCTION*/
    private fun bottomNavigationItemClick() {
        navigation_bar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_1 -> {
                    txt_toolbar.text = "Bảng chữ cái"
                    true
                }
                R.id.nav_2 -> {
                    txt_toolbar.text = "Tập viết"

                    true
                }
                R.id.nav_3 -> {
                    txt_toolbar.text = "Truyện"
                    true
                }
                else -> false
            }
        }
    }
}
