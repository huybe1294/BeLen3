package huypt.com.kotlinandroid.model

 class Stories {
     var title:String?=null
     var description:String?=null
     var htmlSource:String?=null

     constructor()

     constructor(title: String?, description: String?, htmlSource: String?) {
         this.title = title
         this.description = description
         this.htmlSource = htmlSource
     }
 }